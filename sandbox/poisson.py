from fenics import *

# Create mesh and define function space
mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define boundary condition
bc = DirichletBC(V, 0.0, DomainBoundary())

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("sin(5*x[0])*cos(3*x[1])")
a = inner(grad(u), grad(v))*dx
L = f*v*dx

# Compute solution
u = Function(V)
solve(a == L, u, bc)

# Plot solution
plot(u, interactive=True)
