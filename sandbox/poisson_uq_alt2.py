# This is the more integrated version of the interface.
# We add a new type Distribution to UFL to allow PDEs
# to be expressed directly in terms of distributions.
# Then the magic happens when some uq_foo function is
# called. This might call the functionality of our
# framework as in alt1.

from fenics import *

# Create distribution for parameters (independent)
q0 = Distribution("normal", mu=0, sigma=1)
q1 = Distribution("normal", mu=1, sigma=2)

# Create distribution for parameters (dependent)
q0, q1 = Distributions("jointstuff", foo=1)

# Create mesh and define function space
mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define boundary condition
bc = DirichletBC(V, 0.0, DomainBoundary())

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
a = q0*inner(grad(u), grad(v))*dx
L = q1*v*dx

# Compute solution
uq_polychaos(a == L, bc, degree=5)
