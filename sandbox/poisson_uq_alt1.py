# This is the black-box variant of the UQ functionality.
# User implements a solver, either as a function or as
# a class (functor), taking parameters as input and 
# returning solution as output.
# 
# Users then sets up the polychaos stuff as in Chaospy
# and Chaospy (or some added functionality to it) will
# repeatedly call the solver.

from fenics import *

def solve(q):

    # Create mesh and define function space
    mesh = UnitSquareMesh(32, 32)
    V = FunctionSpace(mesh, "Lagrange", 1)

    # Define boundary condition
    bc = DirichletBC(V, 0.0, DomainBoundary())

    # Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    f = Expression("sin(5*x[0])*cos(3*x[1])")
    a = q*inner(grad(u), grad(v))*dx
    L = f*v*dx

    # Compute solution
    u = Function(V)
    solve(a == L, u, bc)

    # Plot solution
    plot(u, interactive=True)

    return u

# Create distribution for parameter q
q_dist = Normal()

# Compute distribution for solution u
u_dist = uq(solve, q_dist)
