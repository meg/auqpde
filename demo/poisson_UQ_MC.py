"""
This program solves Poisson's equation
   - div (alpha * grad u(x,y) ) = f 
on the unit square with Dirichlet boundary 
condition given by u0 = 0 and quantify the 
uncertainity in u(x,y) assuming uncertain 
input parameters: alpha and f are assumed to 
have a known probability density function:

  -alpha: uniform distribution (lower=3.3,upper= 3.5)
  -f: normal distribution (mean=1,sigma=0.1)

 ** More probability density functions: pg 15-16, 
    Chaospy theory guide

Numerical Uncertainity Quantification Method used:
  quasi-Monte Carlo/Halton sequence ("H")

"""

# Copyright (C) 2016 Rocio Rodriguez Cantano
# 2016-06-13




from dolfin import *
import chaospy as cp
import numpy as np
import matplotlib.pyplot as plt




# Create mesh and define function space
mesh = UnitSquareMesh(6, 6)
V = FunctionSpace(mesh, 'Lagrange', 1)

# Define boundary condition
u0 = Constant(0.0)
def u0_boundary(x, on_boundary):
    return on_boundary 

Dbc = DirichletBC(V, u0, u0_boundary)


# Define random distribution for each parameter  
alpha_dist = cp.Uniform(lo=3.3,up=3.5)
f_dist = cp.Normal(1,0.1)


# Joint probability density function
joint= cp.J(alpha_dist,f_dist)


# The model solver
def model_wrapper(Vfs,bc,alpha,f):
        # Define test and trial spaces
	u = TrialFunction(Vfs)
	v = TestFunction(Vfs)
        
        # Define variational problem
	L = f*v*dx
        a = alpha*inner(nabla_grad(u), nabla_grad(v))*dx

        # Compute solution
	u = Function(V)
	solve(a == L, u, bc)
        u_array = u.vector().array()

        return u_array


# Monte Carlo 
nsamples=1000
samples= joint.sample(nsamples, rule="H")
u_mc = [model_wrapper(V,Dbc,*s) for s in samples.T]
       

# output  metrics
mean = np.mean(u_mc, 0)
std = np.std(u_mc,0)       



 


